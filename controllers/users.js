const { response } = require('express');
const bcrypt = require('bcryptjs');

const User = require('../models/User');

const getUsers = async(req,res) => {

    const users = await User.find({}, 'name email role google ');
                                    // tomando los datos que yo deseo obtener
    res.json({
        ok: true,
        users 
    });
}

// ------------ CREAR USUARIO-----------------------------------------------
const createUser = async(req,res = response) => {

    // console.log(req.body);
    // obtener el cuerpo de la solicitud

    const { email, password } = req.body;

    // ahora se atrapa los errores que se obtuvieron en el middleware


    try {

        // toma el email
        const emailUnique = await User.findOne({email});

        // valida si existe
        if(emailUnique){
            return res.status(400).json({
                ok: false,
                msg: 'El correo ya está registrado'
            })
        }

        //  atrapa los datos
        const user = new User(req.body);
    
        // encripta contraseña
        const salt = bcrypt.genSaltSync();

        user.password = bcrypt.hashSync( password, salt );

        // guardo usuario
        await user.save();
        
    
        // el res.json solo se puede mandar una vez en un bloque de codigo...
        res.json({
            ok: true,
            user
        });
        
        // en caso de error...
    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok:false,
            msg: 'error inesperado, revisar logs'
        });
    }

}


//---------------------- ACTUALIZAR USUARIO----------------------------------------------
const updateUser = async( req, res = response ) => {

    // VALIDAR TOKEN Y COMPROBAR SI ES EL USUARIO CORRECTO

    const uid = req.params.id;


    try {

        const userDB = await User.findById(uid);

        if( !userDB ){
            return res.status(404).json({
                ok: false,
                msg: 'No existe usuario con este ID'
            })
        }

        // actualizo campos
        const { password, google, email, ...updateFields} = req.body;        

        if(userDB.email !== email){
            const emailExists = await User.findOne({ email });
            
            if(emailExists){
                return res.status(400).json({
                    ok:false,
                    msg: 'Ya existe un usuario con ese email'
                });
            } 
        }

        updateFields.email = email;

        const updateUser = await User.findByIdAndUpdate(uid, updateFields, { new: true });
                                                    // le pasamos el id del usuario y luego los campos que
                                                    // se desean actualizar 
        
        res.json({
            ok:true,
            user: updateUser
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok:false,
            msg:'Error inesperado'
        });
    }
}
// -----------------------ELIMINAR USUARIO-----------------------------------------------------------
const deleteUser = async( req, res = response ) => {
    
        const uid = req.params._id;


    try {

        const userDB = await User.findById(uid);

        if( !userDB ){
            return res.status(404).json({
                ok: false,
                msg: 'No existe usuario con este ID'
            })
        }

        await User.remove(uid);

        res.json({
            ok:true,
            msg: 'Usuario eliminado'
        });
        
    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok:false,
            msg:'Error inesperado'
        });
    }
}




module.exports = {
    getUsers,
    createUser,
    updateUser,
    deleteUser
}
/*
        IMPORTANTE
    Ruta: /api/usuarios 

*/
const { Router } = require("express");
const { check } = require("express-validator");

const { fieldsValidation } = require("../middlewares/fieldsValidation");
const { getUsers,createUser,updateUser,deleteUser } = require("../controllers/users");
    



const router = Router();

router.get('/', getUsers);

router.post('/', 
    [
        check('name', 'El nombre es obligatorio*').not().isEmpty(),
        check('password', 'La contraseña es obligatoria*').not().isEmpty(),
        check('email', 'El correo electrónico es obligatorio*').isEmail(),
        // luego de chequeo de campos llamamos nuestro middleware personalizado fieldsValidation
        fieldsValidation,
    ], 
        createUser
);

router.put('/:id', 
    [
        check('name', 'El nombre es obligatorio*').not().isEmpty(),
        check('email', 'El correo electrónico es obligatorio*').isEmail(),
        // check('role', 'El rol es obligatorio').isEmail(),
        // luego de chequeo de campos llamamos nuestro middleware personalizado fieldsValidation
        fieldsValidation,
    ], 
        updateUser
);

router.delete('/:id', 
        deleteUser
);


module.exports = router;
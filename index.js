require('dotenv').config();

const express = require('express');
const cors = require('cors')
const { dbConnection } = require('./database/config');

// crear servidor de express
const app = express();

// config de CORS
app.use(cors())

// lectura y parseo del body
// esto siempre va antes de las rutas    
app.use(express.json());


// llamado a base de datos 
dbConnection();

// variable de entorno en el .env
// console.log(process.env); <-- acá verás en consola todas las variables de entorno que hay

// ruta

app.use('/api/usuarios', require('./routes/users') )





app.listen(process.env.PORT, () =>{
    console.log('Servidor corriendo en el puerto', process.env.PORT);
});
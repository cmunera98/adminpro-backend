const { Schema, model } = require("mongoose");

const UserSchema = Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    img: {
        type: String,        
    },
    role: {
        type: String,
        required: true,
        default: 'USER_ROLE'
    },
    google: {
        type: Boolean,
        default: false
    },  
});

UserSchema.method('toJSON', function(){
    // barriendo con los campos de __v y _id 
    const { __v, _id, password, ...object } = this.toObject();

    // volviendo a obtener el id pero con otro nombre uid
    object.uid = _id;

    // esto es para fines visuales y no afecta con la base de datos
    return object;
})

module.exports = model('User', UserSchema);
